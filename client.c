#include<stdio.h> //printf
#include<stdlib.h>
#include<unistd.h>
#include<string.h>    //strlen
#include<sys/types.h>
#include<sys/socket.h>    //socket
#include<netinet/in.h> //inet_addr
#include <netdb.h>

int main(int argc, char *argv[]) {
    int sock, on=1;
    struct sockaddr_in6 server_addr;
    struct hostent *server;
    char message[] = "Hello, server", server_reply[2000];

    if (argc != 3) /* Test for correct number of arguments */ {
        fprintf(stderr, "Usage: %s <Server IP> <Echo Port>\n", argv[0]);
        exit(1);
    }

    //Create socket
    sock = socket(AF_INET6, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("Could not create socket\n");
        abort();
    }
    printf("Socket created\n");
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char* ) &on, sizeof(on)) < 0)
    {
        printf("setsockopt");
        abort();
    }
    memset(&server_addr, 0, sizeof(server_addr));
    
    inet_pton(AF_INET6, argv[1], &server_addr.sin6_addr);
    server_addr.sin6_family = AF_INET6;
    server_addr.sin6_port = htons(atoi(argv[2]));
    
    //Connect to remote server
    if (connect(sock, (struct sockaddr *) &server_addr, sizeof (server_addr)) < 0) {
        perror("connect failed. Error\n");
        return 1;
    }

    printf("Connected\n");

    //keep communicating with server
    while (1) {
        //Send some data
        if (send(sock, message, strlen(message), 0) < 0) {
            printf("Send failed\n");
            close(sock);
            return 1;
        }

        //Receive a reply from the server
        if (recv(sock, server_reply, 2000, 0) < 0) {
            printf("recv failed\n");
            break;
        }

        printf("Server reply: %s", server_reply);
    }

    close(sock);
    return 0;
}