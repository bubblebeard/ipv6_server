#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<sys/types.h>
#include<netdb.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread

//the thread function
void *connection_handler(void *);

int main(int argc, char *argv[]) {
    int socket_desc, client_sock, c, *new_sock;
    struct sockaddr_in6 server, client;
    
    char reply[] = "Hello, client";
    if (argc != 2) /* Test for correct number of arguments */ {
        fprintf(stderr, "Usage: %s <Echo Port>\n", argv[0]);
        exit(1);
    }
    //Create socket
    socket_desc = socket(AF_INET6, SOCK_STREAM, 0);
    if (socket_desc == -1) {
        printf("Could not create socket\n");
    }
    printf("Socket created\n");

    //Prepare the sockaddr_in structure
    server.sin6_family = AF_INET6;
    server.sin6_addr = in6addr_any;
    server.sin6_port = htons(atoi(argv[1]));

    //Bind
    if (bind(socket_desc, (struct sockaddr *) &server, sizeof (server)) < 0) {
        //print the error message
        perror("bind failed. Error\n");
        return 1;
    }
    printf("bind done\n");

    //Listen
    listen(socket_desc, 3);
    //Accept and incoming connection
    printf("Waiting for incoming connections...\n");
    c = sizeof (struct sockaddr_in);
    while ((client_sock = accept(socket_desc, (struct sockaddr *) &client, (socklen_t*) & c))) {
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
        new_sock = malloc(1);
        *new_sock = client_sock;

        if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*) new_sock) < 0) {
            perror("could not create thread\n");
            return 1;
        }

        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( sniffer_thread , NULL);
        printf("Handler assigned\n");
    }

    if (client_sock < 0) {
        perror("accept failed\n");
        return 1;
    }

    return 0;
}

/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc) {
    //Get the socket descriptor
    int sock = *(int*) socket_desc;
    int read_size;
    char *message, client_message[2000];
    
    //Receive a message from client
    while ((read_size = recv(sock, client_message, 2000, 0)) > 0) {
        //Send the message back to client
        printf("Client message: %s\n",client_message);
        write(sock, client_message, strlen(client_message));
    }

    if (read_size == 0) {
        printf("Client disconnected\n");
        fflush(stdout);
    } else if (read_size == -1) {
        perror("recv failed\n");
    }

    //Free the socket pointer
    free(socket_desc);

    return 0;
}